**OTUS - CI/CD**

**Task #1**

Регистрация Gitlab Runner и запуск простейшего пайплайна
Цель: Научиться запускать runner, создать простой шаблонный пайплайн
1) сделать репозиторий
2) запустить и привязать к проекту gitlab-runner
3) написать простейший .gitlab-ci.yml пайплайн по примеру с занятия
4) добиться успешного выполнения пайплайна на своём раннере

** 1. Use local system volume mounts to start the Runner container **
docker run -d --name gitlab-runner --restart always \
	-v /srv/gitlab-runner/config:/etc/gitlab-runner \
	-v /var/run/docker.sock:/var/run/docker.sock \
	gitlab/gitlab-runner:latest

** 2. Register the Runner **
docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
	--url "https://gitlab.com/" \
	--registration-token "Fa-VQXYpcV1AzsqH9Pb8" \
	--description "MyDockerServer-Runner" \
	--tag-list "otus,dz1,docker-runner"